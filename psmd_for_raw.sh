#!/bin/bash
#
# This script calcuates PSMD(Peak width of skeletonized mean diffusivity)
# values for each subject. The original script is from 
# https://markvcid.partners.org/markvcid2-protocols-resources 
# and https://www.psmd-marker.com/. 
# original /cbica/home/srinivad/Projects/2023_PSMD/Scripts/PSMD/psmd.sh 
# has been modified to queue wait at tbss step. Also, this script is only for raw dti data
# unlike psmd.sh which is for raw and processed.

# PSMD is an imaging marker based on diffusion tensor imaging, skeletonization of WM tracts,
# and histogram analysis for cerebral small vessel disease (SVD)

# For the PSMD pipeline, cite
# Baykara E, Gesierich B, Adam R, Tuladhar AM, Biesbroek JM, Koek HL, Ropele S, 
# Jouvent E, Alzheimer’s Disease Neuroimaging Initiative (ADNI), Chabriat H, 
# Ertl-Wagner B, Ewers M, Schmidt R, de Leeuw FE, Biessels GJ, Dichgans M, Duering M
# A novel imaging marker for small vessel disease based on skeletonization of white 
# matter tracts and diffusion histograms. Annals of Neurology 2016 (in press)

# For FSL-TBSS:
# Follow the guidelines at http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/TBSS

##########################################################################################
### Timestamps
echo -e "\nRunning commands on          : `hostname`"
echo -e "Start time                   : `date +%F-%H:%M:%S` \n"

help()
{
    cat <<HELP

    This script does the following and works on only raw dti data. If you want to calculate
    psmd for preprocessed data, use original script:
        1. Eddy current correction
        2. DTI fitting to get FA and MD
        3. TBSS steps which aligns them to standard-space WM skeleton.
        4. Provides single value of psmd foe each subject
    File required:
        1. Raw DTI nifti file and corresponding bvalues and bvectors
        2. skeleton mask (within original script)
    Tools used:
        1. FSL
        2. TBSS
    By default, it will not delete any intermediate files from TBSS

#########################################################################################

Usage : $0 [OPTIONS]
    echo ""
    echo "Usage:"
    echo ""
echo "  From unprocessed DTI data (recommended):"
echo "  psmd_for_raw.sh -d DTI-data -b bvals -r bvecs -s skeleton_mask"
echo ""

    Input:

        Req :  -d   < file >    absolute path to input DTI image in compressed nifti format
        Req :  -b   < file >    absolute path to DTI bvalue file
        Req :  -r   < file >    absolute path to DTI bvec file
        Req :  -s   < file >    absolute path to skeleton mask 
        Req :  -o   < path >    absolute path to output dir for intermediate tbss results.
                                delete after checking the results

ERROR : Not enough input arguments !!
HELP
exit 1     
}

## Defining functions

checkFile()
{
    if [ ! -f $1 ]
    then
        echo -e "\nERROR: Input file $1 does not exist! Aborting operations..."
        exit 1
    fi
}

FileExt()
{
    ## This is valid only for nii.gz
    ImgIP=$1

    if [ ! -f $ImgIP ]
    then
        echo -e "\nERROR: Input file $1 does not exist! Aborting operations..."
        exit 1
    fi

    ImgDir="$(cd "$(dirname "$ImgIP")" && pwd -P)"
    ext=${ImgIP##*.}
    bName=`basename ${ImgIP%.${ext}}`

    if [ "$ext" == "gz" ]
    then
        ext=${bName##*.}.${ext}
        bName=`basename ${ImgIP%.${ext}}`
    fi
    echo $ext $bName $ImgDir

}

parse()
{
    while [ -n "$1" ]
    do
        case $1 in 
        -h)
            help;
            shift 1;;
        -d)
            dti_raw=$2;
            shift 2;;
        -b) 
            dti_bval=$2;
            shift 2;;
        -r)
            dti_bvec=$2;
            shift 2;;
        -s)
            mask=$2;
            shift 2;;
        -o)
            outdir=$2;
            shift 2;;
        -h)
            usage
            ;;
        \?)
            echo ""
            echo "ERROR: Invalid option. Type 'psmd.sh -h' for help" >&2
            help;;
    
        :)
	        echo ""
	        echo "ERROR: Script requires an argument. Type 'psmd.sh -h' for help" >&2
	        Echo ""
            exit 1;;
        esac
    done


}

# Check for skeleton_mask
echo -e "Parsing arguments :$*"
parse $*

if [ ! -f $mask ]
then

	echo "ERROR: Skeleton_mask (option -s) not defined. This is mandatory! Type 'psmd.sh -h' for help"
fi

## check for input dti file

if [ -f $dti_raw ]
then 
    temp=$(FileExt $dti_raw)
    InExt=$(echo $temp | awk '{ print $1 }')
    InbName=$(echo $temp | awk '{ print $2 }')
    InDir=$(echo $temp | awk '{ print $3 }')
fi

################################ MAIN ##################################
echo -e "Pipeline for unprocessed DTI"
echo -e "...Step1: Eddy-correcting DTI data (this step will take a few minutes)..."

sub=$(echo ${InDir} | cut -d/ -f9)
echo $sub
echo $InbName
## export FSL type ###

export FSLOUTPUTTYPE=NIFTI_GZ

eddy_correct ${dti_raw} ${outdir}/${InbName}_eddycorr 1

echo -e "...Step2: Running brain extraction using BET..."

bet ${outdir}/${InbName}_eddycorr ${outdir}/${InbName}_eddycorr_nodiff_brain_F -F -m

echo -e "...Step3: Running tensor calculation..."

dtifit -k ${outdir}/${InbName}_eddycorr -o ${outdir}/${InbName}_eddycorr_tensor -m ${outdir}/${InbName}_eddycorr_nodiff_brain_F_mask -r ${dti_bvec} -b ${dti_bval}

faimagefile=${outdir}/${InbName}_eddycorr_tensor_FA.nii.gz
mdimagefile=${outdir}/${InbName}_eddycorr_tensor_MD.nii.gz

mkdir ${outdir}/tbss_fa -pv
cp -v ${faimagefile} ${outdir}/tbss_fa

cd ${outdir}/tbss_fa
tbssfile=$(ls)


echo -e "....TBSS on FA...."
echo -e "...Skeletonizing FA image (this step will take a few minutes)"
echo -e "...Step1: TBSS-prepare FA in TBSS working directory"

tbss_1_preproc ${tbssfile}

echo -e "...Step2: TBSS-apply non-linear registration of all FA into standard space"

## This will be submitted as job by default using fsl_sub. For subsequent steps 
## to wait for this job, hold next process till this job is finished.

tbss_2_reg -T

pause_crit=$( qstat | grep tbss_2_reg);

while [ -n "$pause_crit" ];
do
    pause_crit=$( qstat | grep tbss_2_reg)
    sleep 20
done
echo "Registration is Complete"

echo -e "...Step3: TBSS-create skeletonized image"

tbss_3_postreg -T

echo -e "...Step4: TBSS-project FA to skeleton"

tbss_4_prestats 0.2

echo -e "....TBSS on MD...."
echo -e "...Skeletonizing MD image (this step will take a few minutes)"
echo -e "...Step1: TBSS-prepare MD in TBSS working directory"

newname=$(ls origdata)
mkdir MD -pv
cp -v ${mdimagefile} MD/${newname}

tbss_non_FA MD

### Histogram analysis ####

echo -e "...PSMD calculation..."

fslmaths stats/all_MD_skeletonised.nii.gz -mas ${mask} -mul 1000000 temp_skel_mask.nii.gz

mdskel=temp_skel_mask.nii.gz

p95=$(fslstats ${mdskel} -P 95)
p5=$(fslstats ${mdskel} -P 5)
psmd=$(echo - | awk "{ print ( ${p95} - ${p5} )/ 1000000}" | sed 's/,/./')

echo $psmd
